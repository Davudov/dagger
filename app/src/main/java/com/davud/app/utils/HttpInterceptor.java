package com.davud.app.utils;

import android.util.Log;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class HttpInterceptor implements Interceptor {

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();

        long t1 = System.nanoTime();
        Log.i("Http request:", request.url().toString());

        Response response = chain.proceed(request);
        long t2 = System.nanoTime();
        Log.i("Http request:", response.body().string());
        return response;
    }
}
