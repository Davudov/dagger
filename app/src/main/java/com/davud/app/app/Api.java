package com.davud.app.app;

import java.util.Map;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public interface Api {
    @GET("fdsnws/event/1/query?format=geojson")
    Single<String> getString(@QueryMap Map<String, String> map);

}
