package com.davud.app.app;

public class Constants {
    public static final int CAM_REQUEST = 1313 ;
    public static final int ERROR_DIALOG_REQUEST = 9001 ;
    public static final int PERMISSIONS_REQUEST_ENABLE_GPS = 9002;
    public static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 9003;
    public static final String EXTRA_PLACE_ITEM = "selected_place" ;
    public static String STR_Separator = "__,__";
    public static String NAVIGATION_PROVIDER = "navigation_provider";
}
