package com.davud.app.app;

import android.app.Application;
import android.os.Bundle;
import android.util.Log;

import com.davud.app.di.DI;
import com.davud.app.di.module.DatabaseModule;
import com.davud.app.di.module.NavigationModule;
import com.davud.app.entity.PlaceData;
import com.davud.app.ui.ItemFragment;
import com.davud.app.ui.MapFragment;

import toothpick.Scope;
import toothpick.Toothpick;
import toothpick.configuration.Configuration;

import static com.davud.app.app.Constants.EXTRA_PLACE_ITEM;

public class App extends Application {
    private static final String TAG = "App";

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate: Application created");

        Toothpick.setConfiguration(Configuration.forProduction());

        Scope appScope = Toothpick.openScope(DI.APP);
        appScope.installModules(new NavigationModule());
        appScope.installModules(new DatabaseModule(this));
    }

    public static ItemFragment getNewInstanceItemFragment(PlaceData data) {
        ItemFragment fragment = new ItemFragment();
        Bundle args = new Bundle();
        args.putParcelable(EXTRA_PLACE_ITEM, data);
        fragment.setArguments(args);

        return fragment;
    }

    public static MapFragment getNewInstanceMapFragment(PlaceData data) {
        MapFragment fragment = new MapFragment();
        Bundle args = new Bundle();
        args.putParcelable(EXTRA_PLACE_ITEM, data);
        fragment.setArguments(args);

        return fragment;
    }

}
