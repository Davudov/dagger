package com.davud.app.app;

public class Screens {
    public static final String MAIN = "main";
    public static final String INSERT = "insert";
    public static final String PROFILE = "profile";
    public static final String SIMPLE_ITEM = "fragment_item";
    public static final String MAP_FRAGMENT = "fragment_map";

}
