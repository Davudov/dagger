package com.davud.app.entity;

public class PhotoUri {
    private String uri;

    public String getUri() {
        return uri;
    }

    public PhotoUri(String uri) {
        this.uri = uri;
    }
}
