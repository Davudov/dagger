package com.davud.app.entity;

import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import io.reactivex.Flowable;

@android.arch.persistence.room.Dao
public interface MyDao {

    @Insert
    void insertPlace(PlaceData ... placeData);


    @Query("SELECT * FROM places")
    Flowable<List<PlaceData>> findAll();



    @Query("delete from places")
    void deleteAllPlaces();

}
