package com.davud.app.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

@Entity(tableName = "places")
public class PlaceData implements Parcelable {
    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "title")
    private String title;

    @ColumnInfo(name = "comment")
    private String comment;

    @ColumnInfo(name = "timestamp")
    private long timestamp;

    @ColumnInfo(name = "uri")
    private String uri;


    @ColumnInfo(name = "longitude")
    private double longitude;



    @ColumnInfo(name = "latitude")
    private double latitude;


    public PlaceData(String title, String comment, long timestamp, String uri, double longitude, double latitude) {
        this.longitude = longitude;
        this.latitude = latitude;
        this.title = title;
        this.comment = comment;
        this.timestamp = timestamp;
        this.uri = uri;
    }


    protected PlaceData(Parcel in) {
        id = in.readInt();
        title = in.readString();
        comment = in.readString();
        timestamp = in.readLong();
        uri = in.readString();
        longitude = in.readDouble();
        latitude = in.readDouble();
    }

    public static final Creator<PlaceData> CREATOR = new Creator<PlaceData>() {
        @Override
        public PlaceData createFromParcel(Parcel in) {
            return new PlaceData(in);
        }

        @Override
        public PlaceData[] newArray(int size) {
            return new PlaceData[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getComment() {
        return comment;
    }


     long getTimestamp() {
        return timestamp;
    }


    public String getUri() {
        return uri;
    }


    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(long longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(title);
        dest.writeString(comment);
        dest.writeLong(timestamp);
        dest.writeString(uri);
        dest.writeDouble(longitude);
        dest.writeDouble(latitude);
    }


}
