package com.davud.app.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.davud.app.R;
import com.davud.app.entity.PlaceData;

import java.util.List;

public class PlacesAdapter extends RecyclerView.Adapter<PlacesAdapter.MViewHolder> {
    private ItemClickListener itemClickListener;

    private List<PlaceData> arrayList;

    public PlacesAdapter(List<PlaceData> arrayList, ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public MViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.simple_list_item_places, viewGroup, false);
        return new MViewHolder(view, itemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull MViewHolder mViewHolder, int i) {
        mViewHolder.title.setText(arrayList.get(i).getTitle());

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class MViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ItemClickListener itemClickListener;
        TextView title;

        MViewHolder(@NonNull View itemView, ItemClickListener itemClickListener) {
            super(itemView);
            title = itemView.findViewById(R.id.title);
            this.itemClickListener = itemClickListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            itemClickListener.onItemClick(getAdapterPosition());
        }
    }
}
