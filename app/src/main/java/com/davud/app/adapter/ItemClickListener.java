package com.davud.app.adapter;

public interface ItemClickListener {
     void onItemClick(int position);
}
