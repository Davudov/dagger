package com.davud.app.adapter;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.davud.app.R;
import com.davud.app.entity.PhotoUri;

import java.util.ArrayList;

public class ImageInsertAdapter extends RecyclerView.Adapter<ImageInsertAdapter.mViewHolder> {
    private ArrayList<PhotoUri> arrayList;

    public ImageInsertAdapter(ArrayList<PhotoUri
            > arrayList) {
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public mViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.simple_list_item, viewGroup, false);
        return new mViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull mViewHolder mViewHolder, int i) {
        String uri = arrayList.get(i).getUri();
        //Working for scale image as aspect ratio
        Bitmap bitmapImage = BitmapFactory.decodeFile(uri);
        if (bitmapImage != null) {
            int nh = (int) (bitmapImage.getHeight() * (512.0 / bitmapImage.getWidth()));
            Bitmap scaled = Bitmap.createScaledBitmap(bitmapImage, 512, nh, true);
            mViewHolder.imageView.setImageBitmap(scaled);
        }
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class mViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;

        mViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.image);


        }
    }

}
