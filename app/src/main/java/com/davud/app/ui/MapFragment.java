package com.davud.app.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.davud.app.R;
import com.davud.app.moxy.map.MapFragmentPresenter;
import com.davud.app.moxy.map.MapFragmentView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapFragment extends MvpAppCompatFragment implements MapFragmentView, OnMapReadyCallback {
    @InjectPresenter
    MapFragmentPresenter fragmentPresenter;
    @ProvidePresenter
    MapFragmentPresenter mapFragmentPresenter(){
        return new MapFragmentPresenter(this);
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_map,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View v, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        double latitude = fragmentPresenter.getCurrentPlace().getLatitude();
        double longitude = fragmentPresenter.getCurrentPlace().getLongitude();
        // Add a marker in Sydney and move the camera
        LatLng place = new LatLng(latitude,longitude);
        googleMap.addMarker(new MarkerOptions().position(place).title("Marker in Selected place"));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(place));
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.getUiSettings().setCompassEnabled(true);
    }
}
