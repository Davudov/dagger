package com.davud.app.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.davud.app.R;
import com.davud.app.adapter.ViewPagerAdapter;
import com.davud.app.entity.PlaceData;
import com.davud.app.moxy.item.ItemPresenter;
import com.davud.app.moxy.item.ItemView;

public class ItemFragment extends MvpAppCompatFragment implements ItemView {
    TextView title, comment, longitude, latitude;
    ViewPager viewPager;
    LinearLayout sliderDotspanel;
    private int dotscount;
    private ImageView[] dots;
    @InjectPresenter
    ItemPresenter itemPresenter;
    FloatingActionButton button;
    @ProvidePresenter
    ItemPresenter itemPresenter() {
        return new ItemPresenter(this);
    }

    ;

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_item, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        title = view.findViewById(R.id.title);
        comment = view.findViewById(R.id.comment);
        longitude = view.findViewById(R.id.longitude);
        latitude = view.findViewById(R.id.latitude);
        viewPager = view.findViewById(R.id.viewPager);
        sliderDotspanel = view.findViewById(R.id.SliderDots);
        button = view.findViewById(R.id.view_in_map);

    }

    @Override
    public void setView(PlaceData selectedPlace) {
        title.setText(selectedPlace.getTitle());
        comment.setText(selectedPlace.getComment());
        latitude.setText(String.valueOf(selectedPlace.getLatitude()));
        longitude.setText(String.valueOf(selectedPlace.getLongitude()));
    }

    @Override
    public void prepareAdapter(ViewPagerAdapter viewPagerAdapter) {
        viewPager.setAdapter(viewPagerAdapter);
    }

    @Override
    public void prepareDotsPanel(ViewPagerAdapter viewPagerAdapter) {
        dotscount = viewPagerAdapter.getCount();
        dots = new ImageView[dotscount];

        for (int i = 0; i < dotscount; i++) {

            dots[i] = new ImageView(getContext());
            dots[i].setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.nonactive_dot));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            params.setMargins(8, 0, 8, 0);

            sliderDotspanel.addView(dots[i], params);

        }

        dots[0].setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.active_dot));

    }

    @Override
    public void addOnPageChangeListener() {

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                for (int i = 0; i < dotscount; i++) {
                    dots[i].setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.nonactive_dot));
                }

                dots[position].setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.active_dot));

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    @Override
    public void openMapView() {
        button.setOnClickListener(v -> {
            itemPresenter.openMapView();
        });
    }

}
