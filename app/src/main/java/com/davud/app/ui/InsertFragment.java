package com.davud.app.ui;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.davud.app.app.Constants;
import com.davud.app.R;
import com.davud.app.adapter.ImageInsertAdapter;
import com.davud.app.entity.PhotoUri;
import com.davud.app.entity.PlaceData;
import com.davud.app.moxy.insert.InsertPresenter;
import com.davud.app.moxy.insert.InsertView;
import com.google.android.gms.common.GoogleApiAvailability;

import java.util.ArrayList;
import java.util.Objects;

import static android.app.Activity.RESULT_OK;
import static com.davud.app.app.Constants.CAM_REQUEST;
import static com.davud.app.app.Constants.ERROR_DIALOG_REQUEST;
import static com.davud.app.app.Constants.PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION;
import static com.davud.app.app.Constants.PERMISSIONS_REQUEST_ENABLE_GPS;

public class InsertFragment extends MvpAppCompatFragment implements InsertView, View.OnClickListener {

    private static final String TAG = "InsertFragment";

    @InjectPresenter
    InsertPresenter insertPresenter;

    @ProvidePresenter
    InsertPresenter presenter() {
        return new InsertPresenter(this.getContext());
    }


    //variables
    long time;
    PlaceData mPlace;
    boolean mLocationPermissionGranted = false;

    //Components
    FloatingActionButton addButton;
    RecyclerView recyclerView;
    ImageInsertAdapter adapter;
    TextInputEditText inputComment;
    TextInputEditText inputTitle;
    ImageView image;
    Button save_btn;
    String comment, title;
    ArrayList<PhotoUri> arrayList = new ArrayList<>();

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_insert, container, false);
        bindViews(view);
        return view;
    }


    private void bindViews(View view) {
        image = view.findViewById(R.id.images_view);
        save_btn = view.findViewById(R.id.save_btn);
        recyclerView = view.findViewById(R.id.recycler_view);
        inputComment = view.findViewById(R.id.input_comment);
        inputTitle = view.findViewById(R.id.input_title);
        addButton = view.findViewById(R.id.add_image);
    }

    public void handleTexts() {
        comment = Objects.requireNonNull(inputComment.getText()).toString();
        title = Objects.requireNonNull(inputTitle.getText()).toString();
    }

    @Override
    public void handleClicks() {
        save_btn.setOnClickListener(this);
        addButton.setOnClickListener(this);
    }


    @Override
    public void setLayoutManager() {
        if (recyclerView != null) {
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        }
    }

    @Override
    public void getUnixTime(long unixTime) {
        time = unixTime;
    }

    @Override
    public void isServiceFail() {
        Toast.makeText(getContext(), "Service Fail message", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSuccessGeoService(double latitude, double longitude) {
        Toast.makeText(getContext(), "Latitude: " + latitude + "\nLongitude: " + longitude, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void isServiceOK(int available) {
        Dialog dialog = GoogleApiAvailability.getInstance().getErrorDialog(getActivity(), available, ERROR_DIALOG_REQUEST);
        dialog.show();
    }

    @Override
    public void setEnableGps(Intent enableGps) {
        startActivityForResult(enableGps, PERMISSIONS_REQUEST_ENABLE_GPS);
    }


    @Override
    public void setPermissions() {
        if (Build.VERSION.SDK_INT >= 23) {
            requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult: called.");
        if (requestCode == CAM_REQUEST && resultCode == RESULT_OK) {
            image.setVisibility(View.GONE);
            String pathToFile = insertPresenter.photoFile.getAbsolutePath();
            arrayList.add(new PhotoUri(pathToFile));
            adapter = new ImageInsertAdapter(arrayList);
            recyclerView.setAdapter(adapter);

        }
        if (requestCode == PERMISSIONS_REQUEST_ENABLE_GPS) {
            if (!mLocationPermissionGranted) {
                getLocationPermission();
            }
        }

    }

    private void getLocationPermission() {
        if (ContextCompat.checkSelfPermission(Objects.requireNonNull(getContext()),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;


        } else {
            ActivityCompat.requestPermissions(Objects.requireNonNull(getActivity()),
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    @Override
    public void onDataSuccess() {
        Toast.makeText(getActivity(), "Place saved successfully", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void takePictureIntent(Intent takePictureIntent) {
        startActivityForResult(takePictureIntent, CAM_REQUEST);
    }

    @Override
    public void onDataFailure() {
        Toast.makeText(getActivity(), "Please fill information", Toast.LENGTH_SHORT).show();
    }

    String getUri(){
        String[] s = new String[arrayList.size()];
        StringBuilder str = new StringBuilder();

        for (int i=0; i<arrayList.size(); i++){
            s[i] = arrayList.get(i).getUri();
        }
        for (int i=0; i<s.length; i++){
            str.append(s[i]);
            if(i<s.length-1){
                str.append(Constants.STR_Separator);
            }
        }

        return str.toString();
    }
    @Override
    public void onClick(View v) {

        if (v.getId() == save_btn.getId()) {
            handleTexts();
            if (comment.equals("") || title.equals("")) {
                Toast.makeText(getActivity(), "Please fill information", Toast.LENGTH_SHORT).show();

            } else {
                mPlace = new PlaceData(title,comment,time,getUri(), InsertPresenter.longitude, InsertPresenter.latitude);
                insertPresenter.savePlace(mPlace);
                insertPresenter.onForwardCommandClick();
            }
        }
        if (v.getId() == addButton.getId()) {
            insertPresenter.dispatchTakePictureIntent();
        }
    }

}
