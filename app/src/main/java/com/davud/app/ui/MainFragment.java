package com.davud.app.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.davud.app.R;
import com.davud.app.adapter.ItemClickListener;
import com.davud.app.adapter.PlacesAdapter;
import com.davud.app.entity.PlaceData;
import com.davud.app.moxy.main.MainPresenter;
import com.davud.app.moxy.main.MainView;

import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;

public class MainFragment extends MvpAppCompatFragment implements MainView, ItemClickListener {
    @Nullable
    RecyclerView recyclerView;
    private TextView message;
    PlacesAdapter adapter;
    List<PlaceData> placeData = new ArrayList<>();
    @InjectPresenter
    MainPresenter presenter;

    @ProvidePresenter
    MainPresenter mainPresenter() {
        return new MainPresenter();
    }

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = view.findViewById(R.id.recycler_view);
        message = view.findViewById(R.id.empty_message);
        if (recyclerView != null) {
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        }

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.deleteAllPlaces) {
            presenter.deleteAllPlaces();
        }

        return true;

    }

    @Override
    public void setRecyclerView(List<PlaceData> places) {
        if (places.size() == 0) {
            message.setVisibility(View.VISIBLE);
        }
        placeData = places;
        adapter = new PlacesAdapter(places, this);
        if (recyclerView != null) {
            recyclerView.setAdapter(adapter);
        }

    }

    @Override
    public void emptyListMessage() {
        message.setText(getResources().getString(R.string.empty_list_message));
        message.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onItemClick(int position) {
        presenter.navigatToItem(placeData.get(position));
        Log.d(TAG, "onItemClick: clicked " + position);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.mDisposable.clear();
    }
}
