package com.davud.app.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.davud.app.R;
import com.davud.app.app.App;
import com.davud.app.app.Screens;
import com.davud.app.di.DI;
import com.davud.app.entity.PlaceData;

import javax.inject.Inject;

import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.Router;
import ru.terrakok.cicerone.android.SupportFragmentNavigator;
import toothpick.Scope;
import toothpick.Toothpick;

public class MainActivity extends MvpAppCompatActivity implements
        NavigationView.OnNavigationItemSelectedListener {
    DrawerLayout drawerLayout;
    NavigationView navigationView;

    @Inject
    Router router;
    @Inject
    NavigatorHolder navigatorHolder;
    Navigator navigator = new SupportFragmentNavigator(getSupportFragmentManager(), R.id.fragment_container) {
        @Override
        protected Fragment createFragment(String screenKey, Object data) {
            return getFragment(screenKey, data);
        }

        @Override
        protected void showSystemMessage(String message) {
            Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();

        }

        @Override
        protected void exit() {
            finish();
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Scope scope = Toothpick.openScope(DI.APP);
//        scope.installModules(new NavigationModule());
        Toothpick.inject(this , scope);
        drawerLayout = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);

        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        router.navigateTo(Screens.MAIN);
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_main:
                router.navigateTo(Screens.MAIN);
                break;
            case R.id.nav_insert:
                router.navigateTo(Screens.INSERT);
                break;
            case R.id.nav_profile:
                router.navigateTo(Screens.PROFILE);
                break;
            default:
                router.navigateTo(Screens.MAIN);
        }

        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else
            super.onBackPressed();
    }


    Fragment getFragment(String screen_key, Object data) {
        switch (screen_key) {
            case Screens.MAIN:
                return new MainFragment();
            case Screens.INSERT:
                return new InsertFragment();
            case Screens.SIMPLE_ITEM:
                return App.getNewInstanceItemFragment((PlaceData) data);
            case Screens.MAP_FRAGMENT:
                return App.getNewInstanceMapFragment((PlaceData) data);
            default:
                return new MainFragment();
        }

    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        navigatorHolder.setNavigator(navigator);
    }

    @Override
    protected void onPause() {
        navigatorHolder.removeNavigator();
        super.onPause();
    }


}
