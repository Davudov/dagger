package com.davud.app.moxy.item;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.davud.app.adapter.ViewPagerAdapter;
import com.davud.app.app.Constants;
import com.davud.app.app.Screens;
import com.davud.app.entity.PhotoUri;
import com.davud.app.entity.PlaceData;
import com.davud.app.ui.ItemFragment;

import java.util.ArrayList;
import java.util.Objects;

import javax.inject.Inject;

import ru.terrakok.cicerone.Router;

@InjectViewState
public class ItemPresenter extends MvpPresenter<ItemView> {
    public ItemPresenter() {
    }

    private ItemFragment fragment;
    @Inject
    Router router;
    public ItemPresenter(ItemFragment fragment) {
        this.fragment = fragment;
    }

    private PlaceData getCurrentPlace() {
        if (fragment.getArguments() != null) {
            PlaceData selectedPlace = fragment.getArguments().getParcelable(Constants.EXTRA_PLACE_ITEM);
            getViewState().setView(selectedPlace);
            return fragment.getArguments().getParcelable(Constants.EXTRA_PLACE_ITEM);
        }
        return null;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        setAdapter();
        addOnPageListener();
        getCurrentPlace();
        getViewState().openMapView();
    }
    private PlaceData getData(){
        return getCurrentPlace();
    }


    private void setAdapter() {
        String uri = Objects.requireNonNull(getCurrentPlace()).getUri();
        ArrayList<PhotoUri> arrayList = new ArrayList<>();
        arrayList.add(new PhotoUri(uri));
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(fragment.getContext(), arrayList);
        getViewState().prepareAdapter(viewPagerAdapter);
        setDotsPanel(viewPagerAdapter);
    }

    private void setDotsPanel(ViewPagerAdapter viewPagerAdapter) {
        getViewState().prepareDotsPanel(viewPagerAdapter);

    }

    private void addOnPageListener() {
        getViewState().addOnPageChangeListener();

    }
    public void openMapView(){
        PlaceData data = getData();
        router.navigateTo(Screens.MAP_FRAGMENT, data);
    }

}

