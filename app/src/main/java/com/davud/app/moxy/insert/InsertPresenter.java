package com.davud.app.moxy.insert;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.davud.app.app.Screens;
import com.davud.app.entity.PlaceData;
import com.davud.app.room.Repository;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

import javax.inject.Inject;

import ru.terrakok.cicerone.Router;

import static android.os.Environment.getExternalStoragePublicDirectory;
import static android.support.constraint.Constraints.TAG;

@InjectViewState
public class InsertPresenter extends MvpPresenter<InsertView>{
    public InsertPresenter() {
    }

    @Inject
    Router router;
    @Inject
    Repository repository;
    private Context context;
    public File photoFile;
    private FusedLocationProviderClient mfusedLocationClient;
    public static double longitude,latitude;
    public InsertPresenter(Context context) {
        this.context = context;
        mfusedLocationClient = LocationServices.getFusedLocationProviderClient(context);
    }


    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        getViewState().setPermissions();
        getViewState().setLayoutManager();
        getViewState().handleClicks();
        checkMapServices();
        getLastLocation();
    }



    public void onForwardCommandClick() {
        router.navigateTo(Screens.MAIN);
    }

    public void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        PackageManager packageManager = Objects.requireNonNull(context).getPackageManager();
        if (takePictureIntent.resolveActivity(packageManager) != null) {
            // Create the File where the photo should go
            photoFile = null;
            photoFile = createImageFile();
            Uri photoURI = FileProvider.getUriForFile(context,
                    "com.davud.app",
                    photoFile);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
            getViewState().takePictureIntent(takePictureIntent);
        }

    }

    private File createImageFile() {
        // Create an image file name
        String pattern = "yyyyMMdd_HHmmss";
        Locale current = context.getResources().getConfiguration().locale;
        String time = new SimpleDateFormat(pattern, current).format(new Date());
        File storageDir = getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = null;
        try {
            image = File.createTempFile(time, ".jpg", storageDir);

        } catch (Exception e) {
            Log.d(TAG, "createImageFile: Error!" + e.getMessage());
        }
        return image;
    }


    //Database operations: saving the data
    public void savePlace(PlaceData data) {
        if (data != null) {
            getViewState().getUnixTime(getUnix());
            repository.insert(data);
            getViewState().onDataSuccess();
        } else
            getViewState().onDataFailure();
    }


    private long getUnix() {
        return System.currentTimeMillis();
    }

    //Map service

     private void getLastLocation() {
        Log.d(TAG, "getLastLocation: called");
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context,Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mfusedLocationClient.getLastLocation().addOnCompleteListener(task -> {
            if (task.isSuccessful()){
                Location location = task.getResult();
                if (location != null) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                    Log.d(TAG, "getLastLocation: " + longitude);

                }

            }
        });

    }

    //Checking


    private void checkMapServices(){
        if(isServicesOK()){
            checkMapsEnabled();
        }
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("This application requires GPS to work properly, do you want to enable it?")
                .setCancelable(true)
                .setPositiveButton("Yes", (dialog, id) -> {
                    Intent enableGpsIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    getViewState().setEnableGps(enableGpsIntent);
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    private void checkMapsEnabled(){
        final LocationManager manager = (LocationManager) context.getSystemService( Context.LOCATION_SERVICE );
        if ( !manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
            buildAlertMessageNoGps();
        }
    }



    private boolean isServicesOK(){
        Log.d(TAG, "isServicesOK: checking google services version");

        int available = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(context);

        if(available == ConnectionResult.SUCCESS){
            //everything is fine and the user can make map requests
            Log.d(TAG, "isServicesOK: Google Play Services is working");
            return true;
        }
        else if(GoogleApiAvailability.getInstance().isUserResolvableError(available)){
            //an error occured but we can resolve it
            Log.d(TAG, "isServicesOK: an error occured but we can fix it");
            getViewState().isServiceOK(available);

        }else{
            getViewState().isServiceFail();
        }
        return false;
    }

}
