package com.davud.app.moxy.item;

import com.arellomobile.mvp.MvpView;
import com.davud.app.adapter.ViewPagerAdapter;
import com.davud.app.entity.PlaceData;

public interface ItemView extends MvpView {
    void setView(PlaceData selectedPlace);
    void prepareAdapter(ViewPagerAdapter viewPagerAdapter);
    void prepareDotsPanel(ViewPagerAdapter viewPagerAdapter);
    void addOnPageChangeListener();
    void openMapView();
}
