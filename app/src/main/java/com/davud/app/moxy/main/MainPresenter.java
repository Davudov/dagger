package com.davud.app.moxy.main;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.davud.app.app.Screens;
import com.davud.app.di.DI;
import com.davud.app.entity.PlaceData;
import com.davud.app.room.Repository;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import ru.terrakok.cicerone.Router;
import toothpick.Scope;
import toothpick.Toothpick;

@InjectViewState
public class MainPresenter extends MvpPresenter<MainView> {

    @Inject
    Repository repository;
    @Inject
    Router router;
    public CompositeDisposable mDisposable = new CompositeDisposable();
     public MainPresenter() {
         Scope scope = Toothpick.openScope(DI.APP);

         Toothpick.inject(this, scope);
         getPlaces();
    }


    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        getViewState().emptyListMessage();
    }

    public void deleteAllPlaces(){
        repository.deleteAllPlaces();
    }

    private void getPlaces(){
        mDisposable.add(repository.findAll().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(placeData -> getViewState().setRecyclerView(placeData)));

    }
    public void navigatToItem(PlaceData data){
         router.navigateTo(Screens.SIMPLE_ITEM, data);
    }
}
