package com.davud.app.moxy.insert;

import android.content.Intent;

import com.arellomobile.mvp.MvpView;


public interface InsertView extends MvpView {
    void takePictureIntent(Intent takePictureIntent);
    void onDataSuccess();
    void onDataFailure();
    void setPermissions();
    void setLayoutManager();
    void handleClicks();
    void getUnixTime(long unixTime);
    void isServiceFail();
    void isServiceOK(int available);
    void onSuccessGeoService(double latitude, double longitude);
    void setEnableGps(Intent enableGps);
}
