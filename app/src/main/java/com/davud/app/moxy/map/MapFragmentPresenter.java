package com.davud.app.moxy.map;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.davud.app.app.Constants;
import com.davud.app.entity.PlaceData;
import com.davud.app.ui.MapFragment;

@InjectViewState
public class MapFragmentPresenter extends MvpPresenter<MapFragmentView>{
    private MapFragment fragment;

     public
     MapFragmentPresenter(MapFragment fragment) {
        this.fragment = fragment;
    }

     public PlaceData getCurrentPlace() {
        if (fragment.getArguments() != null) {
            return fragment.getArguments().getParcelable(Constants.EXTRA_PLACE_ITEM);
        }
        return null;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
    }
}
