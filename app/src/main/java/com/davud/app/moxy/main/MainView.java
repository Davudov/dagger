package com.davud.app.moxy.main;

import com.arellomobile.mvp.MvpView;
import com.davud.app.entity.PlaceData;

import java.util.List;

public interface MainView extends MvpView {
    void setRecyclerView(List<PlaceData> places);
    void emptyListMessage();
}
