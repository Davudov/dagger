package com.davud.app.room;

import android.util.Log;

import com.davud.app.entity.MyDao;
import com.davud.app.entity.PlaceData;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.Flowable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class DataSource implements Repository {
    private MyDao myDao;
    private static final String TAG = "DataSource";

    @Inject
    public DataSource(MyDao myDao) {
        this.myDao = myDao;
    }


    @Override
    public void insert(PlaceData placeData) {
        Completable.fromAction(() -> myDao.insertPlace(placeData)).subscribeOn(Schedulers.io())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onComplete() {
                        Log.d(TAG, "onComplete: Place was data inserted successfully!");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "onError: Place data was not inserted");
                    }
                });
    }

    @Override
    public Flowable<List<PlaceData>> findAll() {
        return myDao.findAll();
    }

    @Override
    public void deleteAllPlaces() {
        Completable.fromAction(() -> myDao.deleteAllPlaces()).subscribeOn(Schedulers.io())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onComplete() {
                        Log.d(TAG, "onComplete: Places deleted successfully!");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "onError: Places were not deleted!");

                    }
                });

    }
}
