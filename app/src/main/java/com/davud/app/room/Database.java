package com.davud.app.room;

import android.arch.persistence.room.RoomDatabase;

import com.davud.app.entity.MyDao;
import com.davud.app.entity.PlaceData;

@android.arch.persistence.room.Database(entities = {PlaceData.class}, version = 3, exportSchema = false)
public abstract class Database extends RoomDatabase {
    public abstract MyDao getDao();
}
