package com.davud.app.room;

import com.davud.app.entity.PlaceData;

import java.util.List;

import io.reactivex.Flowable;

public interface Repository {

    void insert(PlaceData placeData);

    Flowable<List<PlaceData>> findAll();


    void deleteAllPlaces();

}
