package com.davud.app.di.module;

import android.content.Context;

import com.davud.app.di.provider.RoomProvider;
import com.davud.app.room.Repository;

import toothpick.config.Module;

public class DatabaseModule extends Module {
    public DatabaseModule(Context context) {
        bind(Repository.class).toProviderInstance(new RoomProvider(context));
    }

}
