package com.davud.app.di.provider;

import android.arch.persistence.room.Room;
import android.content.Context;

import com.davud.app.room.DataSource;
import com.davud.app.room.Database;
import com.davud.app.room.Repository;

import javax.inject.Provider;

public class RoomProvider implements Provider<Repository> {
    private Repository repository;

    public RoomProvider(Context context) {
        repository = new DataSource(getDatabase(context).getDao());
    }

    public static Database getDatabase(Context context) {
        return Room.databaseBuilder(
                context.getApplicationContext(),
                Database.class,
                "db")
                .fallbackToDestructiveMigration()
                .build();
    }


    @Override
    public Repository get() {
        return repository;
    }
}

