package com.davud.app.di.module;

import com.davud.app.app.Api;
import com.davud.app.di.provider.NetProvider;

import toothpick.config.Module;

public class NetModule extends Module {
    public NetModule(String url) {
        bind(Api.class).toProviderInstance(new NetProvider(url));
    }
}
