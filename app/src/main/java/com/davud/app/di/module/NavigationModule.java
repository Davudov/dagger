package com.davud.app.di.module;

import com.davud.app.di.provider.NavigationProvider;

import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.Router;
import toothpick.config.Module;

public class NavigationModule extends Module {
    public NavigationModule() {
        bind(Router.class).toProviderInstance(new NavigationProvider());
        bind(NavigatorHolder.class).toInstance(NavigationProvider.provideNavigatorHolder());
    }
}
